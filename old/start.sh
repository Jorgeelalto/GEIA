# Run the mosquitto server
echo "Running the mosquitto server"
nohup mosquitto -v -c server.conf > logs/server.out &

# Run the message history agent
echo "Running msghist"
nohup python3 agents/msghist.py > logs/msghist.out &
# Sleep is needed so the agent gets ready

# Run the wikipedia search agent
echo "Running wikisearch"
nohup python3 agents/wikisearch.py > logs/wikisearch.out &

# Run the clock agent
echo "Running clock"
nohup python3 agents/clock.py > logs/clock.out &
# Run the template agent, just for debugging
echo "Running template"
nohup python3 agents/template.py > logs/template.out &


# When all the agents are loaded
echo "Startup completed"
# Wait for input to close the system
read -N 1
echo -e "\b"
echo "Stopping the agents"
nohup python3 agents/stop.py > logs/stop.out & 
# Stop the mosquitto server
sleep 2s
echo "Stopping mosquitto"
killall mosquitto
