# Import the GEIA library
from lib.geia import geia
# Import the JSON utils library
from lib import fJSON
# And load an instance of it, specifying the bus identifier of the agent
lib = geia("msghist")

# We want to retrieve all the messages we receive and store them as JSON
# files to keep a history of all the main bus interactions
while True:
    # We process the net for 10 seconds and then we retrieve all the
    # messages
    lib.process()
    newMessages = True
    # While we have new messages,
    #print("Checking for new messages")
    while newMessages:
        # We get one
        msg = lib.retrieve()
        # if that message is a None object, then there are no
        # more messages
        if msg == None:
            #print("There are no more messages")
            newMessages = False
        else:
            print("Retrieved and saving " + str(msg))
            fJSON.save(msg, "msghist/")
