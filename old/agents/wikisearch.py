# Import the GEIA library
from lib.geia import geia
# Import the wikipedia library
import wikipedia as wiki
# Import the time module
import time

# Load an instance, specifying the bus identifier of the agent
lib = geia("wikisearch")

# An example message this agent will be able to process
#d = {
#    "action": "search wikipedia",
#    "content": {
#        "query": "foobar"
#    }
#}

# First we perform a connection to the server
lib.process()
print("Connected to the main bus")

# Retrieving a received message from the main bus
d = lib.retrieve()
print("Retrieving messages for the first time")

# Processing the message network
while True:
    
    # First we do some processing
    lib.process()
    # Now we get one of the received messages if any
    d = lib.retrieve()
    # If we have a new message then we do stuff
    if d is not None:
        print("Retrieved message: " + str(d))

        # First we see if the message is for us
        if d["action"] == "search wikipedia":
            
            print("It seems we have to do a search")
            # In the case it is, we try to return the summary of the
            # wikipedia page about the given query
            try:
                # If it's successful then send that
                r = wiki.summary(d["content"]["query"])
                print("We got something from the search: " + r)

            # If the search results in a disambiguation page
            except DisambiguationError:
                r = "The query does not look very precise."
                print("It looks like a DisambiguationError")

            # If there is no page matching that search query
            except PageError:
                r = "There is nothing about " + d["query"] + "" \
                    " in Wikipedia."
                print("It looks like a PageError")
    
            # In any case, send a message with the response
            finally:
                d["action"] = "say"
                d["content"] = r
                lib.send(d)
                print("I am sending now " + str(d))
