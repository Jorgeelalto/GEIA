# Import the GEIA library
from lib.geia import geia
# And load an instance of it, specifying the bus identifier of the agent
lib = geia("template")

# An example message
d = {
    "action": "send email",
    "content": "empty"
}

# Sending the message to the main bus
lib.send(d)
# Retrieving a received message from the main bus
message = lib.retrieve()
# Processing the message network
while True:
    lib.process()
