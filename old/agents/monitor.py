# Import the GEIA library
from lib.geia import geia
# Import the JSON utils library
from lib import fJSON
# And load an instance of it, specifying the bus identifier of the agent
lib = geia("monitor")

# We want to print all the messages we receive
while True:
    # We process the net for 10 seconds and then we retrieve all the
    # messages
    lib.process()
    newMessages = True
    # While we have new messages,
    while newMessages:
        # We get one
        msg = lib.retrieve()
        # if that message is a None object, then there are no
        # more messages
        if msg == None:
            newMessages = False
        else:
            print(msg)
