# Import the GEIA library
from lib.geia import geia
# Import the time module
import time

# Load an instance, specifying the bus identifier of the agent
lib = geia("clock")

# An example message this agent will be able to process
#d = {
#    "action": "clock",
#    "content": {
#        "query": "tell the time"
#    }
#}

# First we perform a connection to the server
lib.process()
print("Connected to the main bus")

# Retrieving a received message from the main bus
d = lib.retrieve()
print("Retrieving messages for the first time")

# Processing the message network
while True:
    
    # First we do some processing
    lib.process()
    # Now we get one of the received messages if any
    d = lib.retrieve()
    # If we have a new message then we do stuff
    if d is not None:
        print("Retrieved message: " + str(d))

        # First we see if the message is for us
        if d["action"] == "clock":
            
            print("Message with action -clock- received")
            # In this case, we check whats the query

            # If the user wants to see the time
            if d["content"]["query"] == "tell the time":
                # Add some stuff
                d["content"] = "It is "+str(time.asctime(time.localtime()))+"."

            # We add the action to the message
            d["action"] = "say"
            # And send it
            lib.send(d)
            print("I am sending now " + str(d))
