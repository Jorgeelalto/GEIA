# Importing the mqtt stuff as client
import paho.mqtt.client as mqtt
import sys

# Some variables up here to fix most debugging prints when we use
# this code on other agents


# To be executed when the client gets a response from the server when
# they get a successful connection
def on_connect(client, userData, flags, resultCode):
    print("Connected to the server with result code " +
            str(resultCode))
    # Subscribe to the main topic
    client.subscribe("bus")
    client.publish("bus", payload="STOP", qos=2)

# To be executed when we receive a message from the server
def on_message(client, userData, message):

    msg = (str(message.payload)[2:-1])
    print(str("Received: " + msg))
    # If the message is STOP, then
    if msg == "STOP":
        print("Stopping")
        # Disconnect from the server
        client.disconnect()
        # And stop the agent
        sys.exit(0)
	


# Initialization
# Now we create the client
client = mqtt.Client(client_id="stop")
# And we bind the client object functions to our custom ones
client.on_message = on_message
client.on_connect = on_connect
# We do the connection to the server
client.connect("127.0.0.1", 1883, 60)

client.loop_forever()
