# Importing the mqtt stuff as client
import paho.mqtt.client as mqtt
# Needed for exiting the agent
import sys
# Needing for parsing the string into a dictionary
import ast

class geia(object):
    
    # Constructor
    def __init__(self, idn):
    
        # mosquitto ID for the agent
        self.idn = idn
        # Received messages queue
        self.qReceived = []
        # To be sent messages queue
        self.qToSend = []

        # Now we create the client
        self.client = mqtt.Client(client_id=idn)
        # And we bind the client object functions to our custom ones
        self.client.on_message = self.on_message
        self.client.on_connect = self.on_connect
        # We do the connection to the server
        self.client.connect("127.0.0.1", 1883, 60)


    # Send a dictionary to the bus
    def send(self, d):
        # First, check whether the dictionary is correct
        try:
            d = ast.literal_eval(str(d))
            # If it is, add it to the publish queue
            self.qToSend.append(str(d))
        except:
            #print("The message to be sent is not correct")
            return False
            
    # Retrieve a received dictionary from the bus
    def retrieve(self):
        # Check if there are no messages
        if self.qReceived == []:
            #print("There are no new messages to be retrieved")
            return None
        # If there are, return just the first one
        else:
            return ast.literal_eval(str(self.qReceived.pop()))

    # Process the messages so they actually get sent and received
    def process(self, t=2):
        # Get all the items to send
        while len(self.qToSend) > 0:
            # Send the item
            self.client.publish("bus", payload=self.qToSend.pop(), qos=1)
        # And process the network
        self.client.loop(timeout=t)



    # To be executed when the client gets a response from the server when
    # they get a successful connection
    def on_connect(self, client, userData, flags, resultCode):
        print("Connected to the server with result code " +
                str(resultCode))
        # Subscribe to the main topic
        self.client.subscribe("bus")

    # To be executed when we receive a message from the server
    def on_message(self, client, userData, message):

        msg = str(message.payload)[2:-1]
        # If the message is STOP, then
        if msg == "STOP":
            print("Stopping")
            # Disconnect from the server
            self.client.disconnect()
            # And stop the agent
            sys.exit(0)
        # If it's not an stop, then:
        else:
            # We first have to decode the string into a dictionary
            d = ast.literal_eval(msg)
            # And add it to the received messages queue
            self.qReceived.append(d)
