# femtoJSON is licensed under the GNU General Public License 3.0 :D

# Needed to work with JSON files
import json
# Needed to write the name of the JSON publications
from datetime import datetime

     
# Save a new JSON

# If a path is given, like "./" or "etc/", then use it. If not,
# then it is saved in the same directory as the program. In
# Windows you will probably need to use ".\" and "etc\" to
# specify the directory

# If a name is not given, this function will generate a pretty
# large timestamp which will be used as the name of the file.
# This is pretty useful for stuff like debugging and the like

def save(dic, path = "./", name = None):
    
    # If the user does not specify a custom name for the file,
    if name is None:
        # we first get the current time,
        time = datetime.now()
        # now we get the string of the parts of the time we need,
        # this is going to be the name of the published JSON file
        name = str(str(time.year) + str(time.month) +
                   str(time.day) + str(time.hour) +
                   str(time.minute) + str(time.second) +
                   str(time.microsecond))
        
    # After that, we create a file with that name
    with open(path + name + ".json", "w+") as pub:
        # and dump the dictionary in it, with indentation and
        # sorting so a human being can read it in the
        # case that debugging is needed
        json.dump(dic, pub, sort_keys = True, indent = 4)
        

# Loads a file from the specified path and returns the
# resulting dictionary

def load(path):
    # Open the file
    with open(path, "r") as file:
        # Store the dictionary as dic
        dic = json.load(file)    
        # and return it
        return dic