# Import the GEIA library
from lib.geia import geia
import time
# And load an instance of it, specifying the bus identifier of the agent
lib = geia("test")

# An example message
d = {
    "action": "clock",
    "content": {
        "query": "tell the time",
    }
}

# Sending the message to the main bus
lib.send(d)
# Retrieving a received message from the main bus
message = lib.retrieve()
# Processing the message network
while True:
    lib.process()
    lib.send(d)
    time.sleep(3)
