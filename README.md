### Note
This implementation of the project is mostly unmaintained. It was fun but I'm now
looking to create another implementation that does not need to use a third
party messaging server.

# GEIA - Geia Extended Interactive Automata
This is a modular system which communicates and does things. It's fully modular
since you can add modules on the fly without the need to restart the full
program. Built using Mosquitto server and paho-mqtt for Python 3. It will only
run on Linux, and it could probably run on macOS, but it's designed to run
in a server so the main target is the former.

*Please note*: this project is being developed for investigation, experimental
and personal purposes. It's based on the
[Zoe architecture by GUL UC3M](https://github.com/guluc3m/zoe).
  
### Index
1. [Running GEIA](https://gitlab.com/Jorgeelalto/GEIA#running-geia)  
2. [Creating a new agent](https://gitlab.com/Jorgeelalto/GEIA#creating-a-new-agent)  
3. [Message structure](https://gitlab.com/Jorgeelalto/GEIA#message-structure)  
4. [Implemented agents](https://gitlab.com/Jorgeelalto/GEIA#implemented-agents)  
  
## Running GEIA
For the moment, you just have to clone this repository and run ``./start.sh``.
That will create an easy way of stopping the automata (just by pressing a key)
and also keeps a log of the outputs of every agent, just in case you want
to see what they are doing internally.
  
You can also execute ``mosquitto`` and then run the agents separately, just
remember to navigate to the root directory of the repository and execute from
there like:
```
python3 agents/msghist.py
```
That ensures the agents that manage external files, like *msghist* (it saves a 
history of all the messages transmitted over the bus), find all the folders
correctly.

Also note that you should run the agent *stop* to send the STOP signal to all
the agents:
```
python3 agents/stop.py
```
  
### Requirements
The message broker used as a bus is *mosquitto*, you can install it by typing:
```bash
# Debian based
apt-get install mosquitto
# RHEL, CentOS or Fedora
dnf install mosquitto
# Arch based
pacman -S mosquitto
```
Then, there are some Python 3 dependencies you can install with pip. I've added
them separately here so you know what each one does:
```bash
# Mosquitto server connection
pip3 install paho-mqtt
# Wikipedia search engine
pip3 install wikipedia
```
You may need to use the flag ``--user`` when installing the Python modules with
*pip* if you get a permission denied error.
  
## Creating a new agent
Just for the sake of organization and simplicity, you should create small agents
that can fit in one Python script, and store them in the ``agents/`` folder.
  
The basic code needed to connect to the main data bus and start sending and
receiving messages is already in the *template* agent, but you can find a better
explanation here.
```python
# Import the GEIA library
from lib.geia import geia
# Create an instance of it. You will need to specify the bus identifier of
# the agent, that is, the name the agent will use when connecting to the main
# bus through the library
lib = geia("myAgent")

# An example message, you can see the message structure and standard below
d = {
    "action": "send email",
    "content": {
        "subject": "test",
        "to": "gul@gul.uc3m.es",
        "body": "Hello world"
    }
}

# Sending the example message to the main bus
lib.send(d)
# Retrieving a received message from the main bus. If there are no new messages,
# the function will return None
message = lib.retrieve()
# Processing the message network. This will connect the agent to the main bus,
# and will send and receive the messages. If you do not execute this function,
# your messages will not get sent nor received.
lib.process()
# You can specify a timeout in seconds, so the process function executes
# for some time and stops so the logic of the agent can continue
# running. If you do not specify a time, it will take the default value, 2
# seconds. Here we process the network for 15 seconds:
lib.process(15)
```
And that's all the methods the library has.
  
### Some details about the inner workings of the system
The lib has two queues of messages, one for received messages and
another for messages to be sent. The functions *send(x)* and
*retrieve()* just append or pop from their respective queues. Those
queues are then managed in the *process()* function.

Figure out you want to send three messages at the beginning of the code
of your agent before the main loop starts:
```python
# Loading the library
from lib.geia import geia
# Creating a lib instance with the name of the agent
lib = geia("myAgent")

# ...

# Sending the three messages
lib.send(a)
lib.send(b)
lib.send(c)

# ...

# Main loop
while True:
    # ...
    lib.process()
    # ...
``` 
Internally, the send(x) function is appending the dictionaries *a*,
*b* and *c* to the *toSend* queue. When *process()* is called, the queue is
emptied, element by element, as they are sent to the bus.
  
Similarly, imagine you launch an agent, do some operations and
then you call *process()* and then a few *retrieve()*:
```python
# Loading the library
from lib.geia import geia
# Creating a lib instance with the name of the agent
lib = geia("myAgent")

# We do a process() so the agent connects to the bus
lib.process()

# 10 seconds worth of operations
# ...

# Getting the messages
lib.process()
a = lib.retrieve()
b = lib.retrieve()
c = lib.retrieve()

# ...
```
If in those 10 seconds the agent receives any messages, they get stored
in the *received* queue. When you do the *retrieve()*, the returned
dictionary is the result of "popping" the queue (getting and removing
the first element).
  
## Message structure
The messages are Python dictionaries. This is pretty good stuff since they can
store many types of objects, they can be nested, and they are directly related
to JSON files (see [my fJSON library](https://gitlab.com/Jorgeelalto/fJSON)).

The basic structure of the GEIA messages is:
```python
{
    "action": "set reminder",
    "content": {
        "note": "buy cake",
        "month": 2,
        "day": 25,
        "hour": 14,
        "minute": 30
    }
}
```
Every message has the *action* and *content* data. Each agent will know whether
they can use or not the message depending on what the *action* is, but only
the agents that can use it will read the *content* section, and that one is
fully customizable for each agent. In this example, an hypothetical *reminder*
agent is told to set a new reminder to *buy cake* at 14:30 on the 25th of
february.
  
## Implemented agents
  
#### monitor - Bus monitor
It does not run by default when initializing with ``./start.sh``, and so it has
to be run by the user since it is designed just for debugging. It will print
on the screen any message it receives from the main bus.
  
#### msghist - Message history logger
It keeps a history of the messages that have been sent through the main bus,
saving them into *JSON* files in the ``msghist/`` folder, and setting as their
name the timestamp from the date when they were sent.
  
#### wikisearch - Wikipedia search
Accessible by the action "search wikipedia", this just returns the summary of
the Wikipedia page returned by the given search query.
```python
# Example input message
{
    "action": "search wikipedia",
    "content": {
        "query": "foobar"
    }
}

# Example output message
{
    "action": "say",
    "content": "The terms foobar (/ˈfuːbɑːr/), or foo and others are used as..."
}

# The output here has been crippled so it fits, the real output would be longer
```
  
#### clock - Time teller and reminder manager
For the moment, this agent just tells the time when asked. Adding reminders
and informing the user about them is in progress.
```python
# Example input message
{
    "action": "clock",
    "content": {
        "query": "tell the time"
    }
}

# Example output message
{
    "action": "say",
    "content": "It is Sat Nov 25 13:59:44 2017."
}
```

### Planned features
* Weather now and predictions.  
* Reminders: One time reminders for now.  
* Shopping list / Want to buy list.  
