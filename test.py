import queue
import threading

def threadf(q):
    print("Hello, thread")
    q.put("Stone")
    print("Done, thread")

q = queue.Queue()

th = threading.Thread(target=threadf, args=(q,))
th.start()
print(q.get())